
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import com.kms.katalon.core.testobject.TestObject


def static "createSharingRequest.SelectGender.selectGender"(
    	TestObject to	
     , 	TestObject option	) {
    (new createSharingRequest.SelectGender()).selectGender(
        	to
         , 	option)
}
