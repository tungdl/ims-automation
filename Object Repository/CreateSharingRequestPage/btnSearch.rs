<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btnSearch</name>
   <tag></tag>
   <elementGuidId>38330689-3e5c-4709-9e6f-23ab670233be</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(@class,'primary button')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
