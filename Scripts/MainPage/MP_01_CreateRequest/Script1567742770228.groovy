import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://ims.pacs.cf/#/login')

WebUI.waitForElementVisible(findTestObject(null), 0)

WebUI.sendKeys(findTestObject('LoginPage/txbUsername'), 'admin')

WebUI.sendKeys(findTestObject('LoginPage/txbPassword'), 'admin')

WebUI.click(findTestObject('LoginPage/btnLogin'))

WebUI.waitForElementVisible(findTestObject('MainPage/btnCreateSharingRequest'), 0)

WebUI.click(findTestObject('MainPage/btnCreateSharingRequest'))

WebUI.waitForElementVisible(findTestObject('CreateSharingRequestPage/btnReviewSharingRequest'), 0)

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbFullname'), 'BNTEST')

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbDateOfBirth'), '10/09/2010')

WebUI.click(findTestObject('CreateSharingRequestPage/slbGender'))

WebUI.click(findTestObject('CreateSharingRequestPage/optionGender'))

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbBHYT'), '123456789')

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbCMND'), '123456789')

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbPhone'), '123456789')

WebUI.sendKeys(findTestObject('CreateSharingRequestPage/txbAddress'), '123456789')

WebUI.check(findTestObject('CreateSharingRequestPage/cbxPatient'))

WebUI.click(findTestObject('CreateSharingRequestPage/btnAddAllCandidate'))

WebUI.click(findTestObject('CreateSharingRequestPage/slbConceptType'))

WebUI.waitForElementVisible(findTestObject('CreateSharingRequestPage/txbFilterConceptType'), 0)

WebUI.acceptAlert()

