import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('https://ims.pacs.cf/#/login')

WebUI.waitForElementVisible(findTestObject('LoginPage/btnLogin'), 0)

WebUI.sendKeys(findTestObject('LoginPage/txbUsername'), 'admin')

WebUI.sendKeys(findTestObject('LoginPage/txbPassword'), 'admin')

WebUI.click(findTestObject('LoginPage/btnLogin'))

WebUI.waitForElementVisible(findTestObject('MainPage/imgLogo'), 0)

WebUI.verifyElementVisible(findTestObject('MainPage/imgLogo'))

WebUI.closeBrowser()

